SOURCES = v4l-jpeg.c
OBJECTS := $(SOURCES:.c=.o)
EXEC = v4l-jpeg
CFLAGS = -Wall -g $(shell pkg-config --cflags libv4l2)
LIBS = $(shell pkg-config --libs libv4l2)

all: $(EXEC)

.c.o:
	$(CC) -c $(CFLAGS) $(INCLUDES) $<

$(EXEC): $(OBJECTS)
	$(CC) $(OBJECTS) $(LIBS) -o $(EXEC)

clean:
	rm -f *.o $(EXEC)

.PHONY: clean all
