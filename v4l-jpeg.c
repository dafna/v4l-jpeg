#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <linux/videodev2.h>
#include <linux/media.h>
#include <libv4l2.h>

#define MAX_PLANES	3
#define MAX_QUALITY	100

#define __round_mask(x, y) ((__typeof__(x))((y)-1))
#define round_up(x, y) ((((x)-1) | __round_mask(x, y))+1)

struct context {
	int vfd;
	int request_fd;
	int media_fd;

	const char *src_filename;
	const char *dst_filename;
	unsigned int video_node;
	unsigned int quality;

	unsigned int src_fmt_index;
	unsigned int width;
	unsigned int height;
	unsigned int src_filesize;
	void *src_planes_ptr[MAX_PLANES];
	unsigned int src_planes_size[MAX_PLANES];
	unsigned int src_planes_bytesperline[MAX_PLANES];
	unsigned int src_planes;

	void *dst_plane_ptr;
	unsigned int dst_plane_size;
	unsigned int encoded_size;
};

/**
 * struct enc_fmt - information about supported video formats.
 * @name:       Human readable name of the format.
 * @fourcc:     FourCC code of the format. See V4L2_PIX_FMT_*.
 */
struct enc_fmt {
	char *name;
	unsigned int fourcc;
};

/*
 * Supported formats.
 */
static struct enc_fmt enc_fmts[] = {
	/* Four yuv formats supported by HW. */
	{
		.name = "4:2:0 3 planes Y/Cb/Cr (YUV420M)",
		.fourcc = V4L2_PIX_FMT_YUV420M,
	},
	{
		.name = "4:2:0 2 plane Y/CbCr (NV12M)",
		.fourcc = V4L2_PIX_FMT_NV12M,
	},
	{
		.name = "4:2:2 1 plane YUYV (YUYV)",
		.fourcc = V4L2_PIX_FMT_YUYV,
	},
	{
		.name = "4:2:2 1 plane UYVY (UYVY)",
		.fourcc = V4L2_PIX_FMT_UYVY,
	},
};
#define SUPPORTED_FMTS_SIZE (sizeof(enc_fmts) / sizeof(enc_fmts[0]))

static void print_supported_fmts(void)
{
	int i;

	fprintf(stderr, "Supported formats:\n");
	for (i = 0; i < SUPPORTED_FMTS_SIZE; i++) {
		fprintf(stderr, "\t\t%d: %s\n", i, enc_fmts[i].name);
	}
}

void print_usage(void)
{
	fprintf(stderr, "Usage:\n"
		 "-i input file\n"
		 "-o output file\n"
		 "-v device node\n"
		 "-w input width\n"
		 "-h input height\n"
		 "-f input format index: <fmt>\n"
		 "-q JPEG quality: 1-100\n\n");
	print_supported_fmts();
}

static int parse_args(struct context *ctx, int argc, char * const argv[])
{
	int c;

	while((c = getopt(argc, argv, "i:o:v:w:h:f:q:")) != -1) {
		switch(c) {
		case 'i':
			ctx->src_filename = optarg;
			break;
		case 'o':
			ctx->dst_filename = optarg;
			break;
		case 'v':
			ctx->video_node = atoi(optarg);
			break;
		case 'w':
			ctx->width = atoi(optarg);
			break;
		case 'h':
			ctx->height = atoi(optarg);
			break;
		case 'q':
			ctx->quality = atoi(optarg);
			break;
		case 'f':
			ctx->src_fmt_index = atoi(optarg);
			break;
		case '?':
			print_usage();
			return -1;
		default:
			fprintf(stderr,
				 "Unknown option character `-%c'.\n",
				 optopt);
			return -1;
		}
	}

	return 0;
}

static int set_dst_fmt(struct context *ctx)
{
	struct v4l2_format fmt;
	int ret;

	memset(&fmt, 0, sizeof(fmt));

	fmt.type		= V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	fmt.fmt.pix_mp.width	= ctx->width;
	fmt.fmt.pix_mp.height	= ctx->height;
	fmt.fmt.pix_mp.field	= V4L2_FIELD_ANY;
	fmt.fmt.pix_mp.num_planes = 1;
	fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_JPEG;
	fmt.fmt.pix_mp.plane_fmt[0].sizeimage = round_up(ctx->width, 16) * round_up(ctx->height, 16);

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_S_FMT, &fmt);
	if (ret) {
		fprintf(stderr, "set dst fmt failed\n");
		return ret;
	}

	printf("destination plane sizeimage %d bytes\n", fmt.fmt.pix_mp.plane_fmt[0].sizeimage);

	return 0;
}

static int set_src_fmt(struct context *ctx)
{
	struct v4l2_format fmt;
	struct enc_fmt *enc_fmt;
	int ret, i;

	memset(&fmt, 0, sizeof(fmt));
	enc_fmt = &enc_fmts[ctx->src_fmt_index];

	fmt.type		= V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	fmt.fmt.pix_mp.width	= ctx->width;
	fmt.fmt.pix_mp.height	= ctx->height;
	fmt.fmt.pix_mp.pixelformat = enc_fmt->fourcc;
	fmt.fmt.pix_mp.field	= V4L2_FIELD_ANY;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_S_FMT, &fmt);
	if (ret) {
		fprintf(stderr, "set src fmt failed\n");
		return ret;
	}

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_G_FMT, &fmt);
	if (ret) {
		fprintf(stderr, "set src fmt failed\n");
		return ret;
	}

	for (i = 0; i < fmt.fmt.pix_mp.num_planes; i++) {
		ctx->src_planes_size[i] = fmt.fmt.pix_mp.plane_fmt[i].sizeimage;
		ctx->src_planes_bytesperline[i] = fmt.fmt.pix_mp.plane_fmt[i].bytesperline;
		printf("source plane %d sizeimage %d bytes\n", i, fmt.fmt.pix_mp.plane_fmt[i].sizeimage);
	}
	ctx->src_planes = fmt.fmt.pix_mp.num_planes;

	return 0;
}

static int copy_input_file(struct context *ctx, struct v4l2_plane *planes)
{
	void *file_buf;
	int fd, i;

	fd = open(ctx->src_filename, O_RDONLY);
	file_buf = mmap(NULL, ctx->src_filesize, PROT_READ, MAP_SHARED, fd, 0);
	if (file_buf == MAP_FAILED) {
		fprintf(stderr, "map input file failed\n");
		return errno;
	}

	for (i = 0; i < ctx->src_planes; i++) {
		memcpy(ctx->src_planes_ptr[i], file_buf, ctx->src_planes_size[i]);
		planes[i].bytesused = ctx->src_planes_size[i];
		file_buf += ctx->src_planes_size[i];
	}

	munmap(file_buf, ctx->src_filesize);
	close(fd);

	return 0;
}

static int req_src_buf(struct context *ctx)
{
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_plane planes[MAX_PLANES];
	struct v4l2_buffer buf;
	struct v4l2_exportbuffer expbuf;
	int i, ret;

	memset(&reqbuf, 0, sizeof(reqbuf));
	memset(&buf, 0, sizeof(buf));

	reqbuf.count	= 1;
	reqbuf.type	= V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	reqbuf.memory	= V4L2_MEMORY_MMAP;
	ret = v4l2_ioctl(ctx->vfd, VIDIOC_REQBUFS, &reqbuf);
	if (ret) {
		fprintf(stderr, "req src buf failed\n");
		return ret;
	}

	buf.type	= V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= ctx->src_planes;
	buf.m.planes	= planes;
	ret = v4l2_ioctl(ctx->vfd, VIDIOC_QUERYBUF, &buf);
	if (ret) {
		fprintf(stderr, "query src buf failed\n");
		return ret;
	}

	for (i = 0; i < buf.length; i++) {
		memset(&expbuf, 0, sizeof(expbuf));
		expbuf.index = buf.index;
		expbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
		expbuf.flags = O_CLOEXEC | O_RDWR;
		expbuf.plane = i;
		ret = v4l2_ioctl(ctx->vfd, VIDIOC_EXPBUF, &expbuf);
		if (ret) {
			fprintf(stderr, "export src buf failed\n");
			return ret;
		}

		ctx->src_planes_ptr[i] = mmap(NULL, buf.m.planes[i].length,
					      PROT_READ | PROT_WRITE, MAP_SHARED,
					      expbuf.fd, 0);
		if (ctx->src_planes_ptr[i] == MAP_FAILED) {
			fprintf(stderr, "map src plane %d failed\n", i);
			return errno;
		}
		printf("src plane %d mapped %d bytes\n", i, buf.m.planes[i].length);
	}

	copy_input_file(ctx, planes);

	/* queue input buffer */
	memset(&buf, 0, sizeof(buf));
	buf.type	= V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= ctx->src_planes;
	buf.m.planes	= planes;
	buf.flags       = V4L2_BUF_FLAG_REQUEST_FD;
	buf.request_fd  = ctx->request_fd;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_QBUF, &buf);
	if (ret) {
		fprintf(stderr, "Queue src buf failed\n");
		return ret;
	}

	return 0;
}

static int req_dst_buf(struct context *ctx)
{
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_plane plane;
	struct v4l2_buffer buf;
	int ret;

	memset(&reqbuf, 0, sizeof(reqbuf));

	reqbuf.count	= 1;
	reqbuf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	reqbuf.memory	= V4L2_MEMORY_MMAP;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_REQBUFS, &reqbuf);
	if (ret) {
		fprintf(stderr, "req dst buf failed\n");
		return ret;
	}

	memset(&buf, 0, sizeof(buf));
	buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= 1;
	buf.m.planes	= &plane;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_QUERYBUF, &buf);
	if (ret) {
		fprintf(stderr, "query dst buf failed\n");
		return ret;
	}

	/* queue input buffer */
	memset(&buf, 0, sizeof(buf));
	buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= 1;
	buf.m.planes	= &plane;

	memset(&buf, 0, sizeof(buf));
	buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= 1;
	buf.m.planes	= &plane;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_QUERYBUF, &buf);
	if (ret) {
		fprintf(stderr, "query dst buf failed\n");
		return ret;
	}

	ctx->dst_plane_ptr = mmap(NULL, buf.m.planes[0].length,
				  PROT_WRITE | PROT_READ, MAP_SHARED,
				  ctx->vfd, buf.m.planes[0].m.mem_offset);
	if (ctx->dst_plane_ptr == MAP_FAILED) {
		fprintf(stderr, "map dst plane failed\n");
		return errno;
	}
	ctx->dst_plane_size = buf.m.planes[0].length;

	printf("capture buffer size: %d\n", ctx->dst_plane_size);

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_QBUF, &buf);
	if (ret) {
		fprintf(stderr, "Queue dst buf failed\n");
		return ret;
	}

	return 0;
}

int streamon(struct context *ctx)
{
	enum v4l2_buf_type type;
	int ret;

	type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	ret = v4l2_ioctl(ctx->vfd, VIDIOC_STREAMON, &type);
	if (ret) {
		fprintf(stderr, "stream on output failed\n");
		return ret;
	}

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	ret = v4l2_ioctl(ctx->vfd, VIDIOC_STREAMON, &type);
	if (ret) {
		fprintf(stderr, "stream on input failed\n");
		return ret;
	}

	return ret;
}

int dqbuf(struct context *ctx)
{
	struct v4l2_buffer buf;
	struct v4l2_plane planes[MAX_PLANES];
	fd_set fds;
	int ret;

	FD_ZERO(&fds);
	FD_SET(ctx->vfd, &fds);

	ret = select(ctx->vfd + 1, &fds, NULL, NULL, 0);
	if (ret < 0) {
		fprintf(stderr, "select syscall failed\n");
		return ret;
	}

	memset(&buf, 0, sizeof(buf));

	buf.type	= V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= ctx->src_planes;
	buf.m.planes	= planes;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_DQBUF, &buf);
	if (ret) {
		fprintf(stderr, "src dqbuf failed\n");
		return ret;
	}

	memset(&buf, 0, sizeof(buf));
	buf.type	= V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	buf.memory	= V4L2_MEMORY_MMAP;
	buf.index	= 0;
	buf.length	= 1;
	buf.m.planes	= planes;

	ret = v4l2_ioctl(ctx->vfd, VIDIOC_DQBUF, &buf);
	if (ret) {
		fprintf(stderr, "dst dqbuf failed\n");
		return ret;
	}

	ctx->encoded_size = buf.m.planes[0].bytesused;
	printf("capture bytes: %d\n", ctx->encoded_size);

	return 0;
}

#define __round_mask(x, y) ((__typeof__(x))((y)-1))
#define round_up(x, y) ((((x)-1) | __round_mask(x, y))+1)

static int generate_output_file(struct context *ctx)
{
	int fd;

	fd = open(ctx->dst_filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	write(fd, ctx->dst_plane_ptr, ctx->encoded_size);
	printf("%s written, %d bytes\n", ctx->dst_filename, ctx->encoded_size);
	close(fd);
	return 0;
}

static void sanity_check_ctx(struct context *ctx)
{
	int fd;
	struct stat stat;

	assert(ctx->width != 0 && ctx->height != 0);
	assert(ctx->src_filename && ctx->dst_filename);
	assert(ctx->quality <= MAX_QUALITY);
	assert(ctx->src_fmt_index < SUPPORTED_FMTS_SIZE);

	fd = open(ctx->src_filename, O_RDONLY);
	fstat(fd, &stat);
	close(fd);

	ctx->src_filesize = stat.st_size;
}

static int set_quality(struct context *ctx)
{
	struct v4l2_ext_controls ext_ctrls;
	struct v4l2_ext_control ctrls[1];
	int ret;

	memset(&ext_ctrls, 0, sizeof(struct v4l2_ext_controls));
	memset(&ctrls[0], 0, sizeof(struct v4l2_ext_control));

	ctrls[0].id = V4L2_CID_JPEG_COMPRESSION_QUALITY;
	ctrls[0].value = ctx->quality;

	ext_ctrls.count = 1;
	ext_ctrls.controls = ctrls;
	ext_ctrls.which = V4L2_CTRL_WHICH_REQUEST_VAL;
	ext_ctrls.request_fd = ctx->request_fd;

	ret = ioctl(ctx->vfd, VIDIOC_S_EXT_CTRLS, &ext_ctrls);
	if (ret < 0) {
		fprintf(stderr, "set control failed %d\n", errno);
	}

	return ret;
}

int main(int argc, char * const argv[])
{
	int ret, i;
	char video[20];
	struct context ctx;
	struct v4l2_capability cap;

	if (argc < 2) {
		print_usage();
		return 0;
	}
	memset(&ctx, 0, sizeof(ctx));
	ret = parse_args(&ctx, argc, argv);
	if (ret)
		return ret;

	sanity_check_ctx(&ctx);

	sprintf(video, "/dev/media%d", ctx.video_node);
	ctx.media_fd = open(video, O_RDWR | O_NONBLOCK, 0);
	if (ctx.media_fd < 0) {
		fprintf(stderr, "open %s failed\n", video);
		return ctx.vfd;
	}

	sprintf(video, "/dev/video%d", ctx.video_node);
	ctx.vfd = v4l2_open(video, O_RDWR | O_NONBLOCK, 0);
	if (ctx.vfd < 0) {
		fprintf(stderr, "open %s failed\n", video);
		return ctx.vfd;
	}

	ret = v4l2_ioctl(ctx.vfd, VIDIOC_QUERYCAP, &cap);
	if (ret < 0) {
		fprintf(stderr, "querycap failed\n");
		return ret;
	}
	if (!(cap.device_caps & V4L2_CAP_VIDEO_M2M_MPLANE)) {
		fprintf(stderr, "Device %s does not support M2M MPLANE(%#x)\n",
			video, cap.device_caps);
		return -EINVAL;
	}

	ret = ioctl(ctx.media_fd, MEDIA_IOC_REQUEST_ALLOC, &ctx.request_fd);
	if (ret < 0) {
		fprintf(stderr, "unable to allocate media request %d\n", errno);
		return ret;
	}

	ret = set_quality(&ctx);
	if (ret)
		goto cleanup;

	/* Set dst fmt prior to src as setting dst fmt would reset
	 * src fmt in rockchip vpu encoder driver*/
	ret = set_dst_fmt(&ctx);
	if (ret)
		goto cleanup;
	ret = set_src_fmt(&ctx);
	if (ret)
		goto cleanup;
	ret = req_src_buf(&ctx);
	if (ret)
		goto cleanup;
	ret = req_dst_buf(&ctx);
	if (ret)
		goto cleanup;

	ret = ioctl(ctx.request_fd, MEDIA_REQUEST_IOC_QUEUE, NULL);
	if (ret < 0) {
		fprintf(stderr, "unable to queue media request %d\n", errno);
		return ret;
	}

	ret = streamon(&ctx);
	if (ret)
		goto cleanup;

	ret = dqbuf(&ctx);
	if (ret)
		goto cleanup;

	generate_output_file(&ctx);

cleanup:
	v4l2_close(ctx.vfd);

	for (i = 0; i < ctx.src_planes; i++) {
		if (!ctx.src_planes_ptr[i])
			break;
		munmap(ctx.src_planes_ptr[i], ctx.src_planes_size[i]);
	}
	if (ctx.dst_plane_ptr)
		munmap(ctx.dst_plane_ptr, ctx.dst_plane_size);

	return ret;
}
